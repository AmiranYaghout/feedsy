class Publisher < ActiveRecord::Base
  has_many :users, through: :users_publisher
  has_many :users_publisher
  has_many :interests, through: :publishers_interest
  has_many :publishers_interest
  has_many :articles



  def set_user!(user)
    self.user_id = user.id
    self.save!
  end
end

