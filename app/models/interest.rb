class Interest < ActiveRecord::Base
  has_many :users, through: :users_interest
  has_many :users_interest
  has_many :publishers, through: :publishers_interest
  has_many :publishers_interest



  def set_user!(user)
    self.user_id = user.id
    self.save!
  end
end



