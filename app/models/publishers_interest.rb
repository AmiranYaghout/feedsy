class PublishersInterest < ActiveRecord::Base
  belongs_to :publisher
  belongs_to :interest
end
