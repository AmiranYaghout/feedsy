class UsersPublisher < ActiveRecord::Base
  belongs_to :user
  belongs_to :publisher
  accepts_nested_attributes_for :publisher
end
