class InterestPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      user.interests
    end
  end

  def index?
    true
  end

  def show?
    true
  end
  def new?
    true
  end

  def create?
    true
  end

  def edit?
    true
  end

  def update?
    true
  end

  def destroy?
    true
  end

  def find?
    true
  end

end
