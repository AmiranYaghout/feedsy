class SearchController < ApplicationController
  skip_before_action :authenticate_user!

  def find
    @topics = Topic.search_by_name_and_description(params[:search])
    authorize @topics
    @search_query = params[:search]
  end
end
