class PublishersController < ApplicationController
before_action :find_user
after_action  :set_publisher, only: [:create]
before_action :find_interest, only: [:new, :create]





  def index
    @publishers = policy_scope(Publisher)
  end

  def show
    @publisher = Publisher.find(params[:id])
  end

  def new
    @publisher = Publisher.new
    authorize @publisher
  end

  def create
    @publisher = @interest.publishers.build(publisher_params)
    @publishers_interests = PublishersInterest.create(publisher: @publisher, interest: @interest)

    authorize @publisher
    if @publisher.save

      respond_to do |format|
          format.js
          format.html
      flash[:notice] = 'Your publisher has been added'
      end
    else
      respond_to do |format|
        format.js { render }
        format.html { render :new }
      end
    end
  end

  def edit
    @publisher = Publisher.find(params[:id])
    authorize @publisher
  end

  def update
    @publisher = Publisher.find(params[:id])
    @publisher.update(publisher_params)
    authorize @publisher

  end



  private

  def publisher_params
    params.require(:publisher).permit(:publisher, :feed, :id)
  end

  def find_user
    @user = current_user
  end

def set_publisher
  @users_publishers = UsersPublisher.create(user: current_user, publisher: @publisher)
end

def find_interest
  @interest = Interest.find(params[:interest_id])
end




end


