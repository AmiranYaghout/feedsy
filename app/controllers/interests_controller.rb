class InterestsController < ApplicationController
before_action :find_user



  def index
    @interests = policy_scope(Interest)
  end

def show
  @interest = Interest.find(params[:id])
end

def new
@interest = Interest.new
authorize @interest
end

def create
  @interest = Interest.new(interest_params)
  @users_interests = UsersInterest.create(user: current_user, interest: @interest)
  authorize @interest
    if @interest.save
      respond_to do |format|
        format.js
        format.html {redirect_to new_user_interest_publisher_path(interest_id: @interest.id)}

        end
        flash[:notice] = 'Thank you, we will be in touch soon'

    else
      respond_to do |format|
        format.js { render }
        format.html { render :new }
      end
  end
end

def edit
  @interest = Interest.find(params[:id])
  authorize @interest
end

def update
  @interest = Interest.find(params[:id])
  @interest.update(interest_params)
  if @interest.save
    flash[:notice] = 'Your article has been added'

  else
    flash[:notice] = 'Oops something went wrong'
  end
end

def find
  @interest = Interest.find(params[:interest_id])
end


private

  def interest_params
    params.require(:interest).permit(:id, :name, :user_id)
  end

  def find_user
    @user = current_user
  end



end
