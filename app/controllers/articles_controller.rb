class ArticlesController < ApplicationController
before_action :find_topic, only: [:new, :create, :index, :edit, :update]
skip_before_action :authenticate_user!, only: [:index]
skip_before_filter :verify_authenticity_token, only: [:vote]
skip_after_action :verify_authorized, only: [:vote]


def index
  @articles = policy_scope(Article).where(topic: @topic)
end

def new
@article = Article.new
authorize @article
end

def create
  @article = @topic.articles.build(article_params)
  @article.set_user!(current_user)
  @article.votes = 0
  authorize @article
  if
    @article.save
    @article.set_user!(current_user)
    respond_to do |format|
        format.js
        format.html {redirect_to topic_articles_path(@topic.id)}
      end
    flash[:notice] = 'Your article has been added'

  else
    respond_to do |format|
      format.js { render }
      format.html { render :new }
      end

  end
end

def edit
  @article = Article.find(params[:id])
  authorize @article
end

def update
  @article = Article.find(params[:id])
  @article = @topic.articles.update(article_params)
  if @article.save
    flash[:notice] = 'Your article has been added'
    redirect_to topic_articles_path(@topic.id)
  else
    flash[:notice] = 'Oops something went wrong'
  end
end

def vote
  @article = Article.find(params[:article_id])
  @articlevote = Articlevote.where(user: current_user, article: @article)
  if @articlevote.count == 0
    @article.votes += 1

    @article.save
    @articlevote = Articlevote.create(user: current_user, article: @article)
    respond_to do |format|
      format.js
      end
  else
    respond_to do |format|
      format.js {flash.now[:votingerror] = "you have already voted on this article"}
      # { flash.now[:alert] = "You have already voted on this article. Why not check out another article?" }
      end

  end
end




private

def article_params
  params.require(:article).permit(:title, :url,:description, :votes, :comments, :user_id)
end

def find_topic
@topic = Topic.find(params[:topic_id])

end

end
