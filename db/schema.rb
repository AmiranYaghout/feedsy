# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160111215043) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "articles", force: :cascade do |t|
    t.string   "title"
    t.string   "url"
    t.string   "description"
    t.integer  "votes"
    t.string   "comments"
    t.integer  "user_id"
    t.integer  "topic_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "publisher_id"
  end

  add_index "articles", ["publisher_id"], name: "index_articles_on_publisher_id", using: :btree
  add_index "articles", ["topic_id"], name: "index_articles_on_topic_id", using: :btree
  add_index "articles", ["user_id"], name: "index_articles_on_user_id", using: :btree

  create_table "articlevotes", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "article_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "articlevotes", ["article_id"], name: "index_articlevotes_on_article_id", using: :btree
  add_index "articlevotes", ["user_id"], name: "index_articlevotes_on_user_id", using: :btree

  create_table "comments", force: :cascade do |t|
    t.integer  "article_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text     "content"
    t.integer  "user_id"
  end

  add_index "comments", ["article_id"], name: "index_comments_on_article_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "interestpublishers", force: :cascade do |t|
    t.integer  "interest_id"
    t.integer  "publisher_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "interestpublishers", ["interest_id"], name: "index_interestpublishers_on_interest_id", using: :btree
  add_index "interestpublishers", ["publisher_id"], name: "index_interestpublishers_on_publisher_id", using: :btree

  create_table "interests", force: :cascade do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "publisher_id"
  end

  add_index "interests", ["publisher_id"], name: "index_interests_on_publisher_id", using: :btree
  add_index "interests", ["user_id"], name: "index_interests_on_user_id", using: :btree

  create_table "pg_search_documents", force: :cascade do |t|
    t.text     "content"
    t.integer  "searchable_id"
    t.string   "searchable_type"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "pg_search_documents", ["searchable_type", "searchable_id"], name: "index_pg_search_documents_on_searchable_type_and_searchable_id", using: :btree

  create_table "publishers", force: :cascade do |t|
    t.string   "publisher"
    t.string   "feed"
    t.integer  "user_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "interest_id"
  end

  add_index "publishers", ["interest_id"], name: "index_publishers_on_interest_id", using: :btree
  add_index "publishers", ["user_id"], name: "index_publishers_on_user_id", using: :btree

  create_table "publishers_interests", force: :cascade do |t|
    t.integer  "publisher_id"
    t.integer  "interest_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "publishers_interests", ["interest_id"], name: "index_publishers_interests_on_interest_id", using: :btree
  add_index "publishers_interests", ["publisher_id"], name: "index_publishers_interests_on_publisher_id", using: :btree

  create_table "publishers_users", force: :cascade do |t|
    t.integer  "publisher_id"
    t.integer  "user_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "publishers_users", ["publisher_id"], name: "index_publishers_users_on_publisher_id", using: :btree
  add_index "publishers_users", ["user_id"], name: "index_publishers_users_on_user_id", using: :btree

  create_table "topics", force: :cascade do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.text     "description"
    t.boolean  "verified"
  end

  add_index "topics", ["user_id"], name: "index_topics_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "provider"
    t.string   "uid"
    t.string   "picture"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "token"
    t.datetime "token_expiry"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_interests", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "interest_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "users_interests", ["interest_id"], name: "index_users_interests_on_interest_id", using: :btree
  add_index "users_interests", ["user_id"], name: "index_users_interests_on_user_id", using: :btree

  create_table "users_publishers", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "publisher_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "users_publishers", ["publisher_id"], name: "index_users_publishers_on_publisher_id", using: :btree
  add_index "users_publishers", ["user_id"], name: "index_users_publishers_on_user_id", using: :btree

  add_foreign_key "articles", "publishers"
  add_foreign_key "articles", "topics"
  add_foreign_key "articles", "users"
  add_foreign_key "articlevotes", "articles"
  add_foreign_key "articlevotes", "users"
  add_foreign_key "comments", "articles"
  add_foreign_key "comments", "users"
  add_foreign_key "interestpublishers", "interests"
  add_foreign_key "interestpublishers", "publishers"
  add_foreign_key "interests", "publishers"
  add_foreign_key "interests", "users"
  add_foreign_key "publishers", "interests"
  add_foreign_key "publishers", "users"
  add_foreign_key "publishers_interests", "interests"
  add_foreign_key "publishers_interests", "publishers"
  add_foreign_key "publishers_users", "publishers"
  add_foreign_key "publishers_users", "users"
  add_foreign_key "topics", "users"
  add_foreign_key "users_interests", "interests"
  add_foreign_key "users_interests", "users"
  add_foreign_key "users_publishers", "publishers"
  add_foreign_key "users_publishers", "users"
end
