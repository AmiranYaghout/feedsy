class CreatePublishersInterests < ActiveRecord::Migration
  def change
    create_table :publishers_interests do |t|
      t.references :publisher, index: true, foreign_key: true
      t.references :interest, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
