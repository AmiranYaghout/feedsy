class AddPubliseherToArticles < ActiveRecord::Migration
  def change
    add_reference :articles, :publisher, index: true, foreign_key: true
  end
end
