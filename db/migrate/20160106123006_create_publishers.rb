class CreatePublishers < ActiveRecord::Migration
  def change
    create_table :publishers do |t|
      t.string :publisher
      t.string :feed
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
