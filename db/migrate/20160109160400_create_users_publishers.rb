class CreateUsersPublishers < ActiveRecord::Migration
  def change
    create_table :users_publishers do |t|
      t.references :user, index: true, foreign_key: true
      t.references :publisher, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
